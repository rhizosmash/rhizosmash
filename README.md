rhizoSMASH - the *rhizo*bacterial cataboli*SM* *A*nalysis *SH*ell
===================================================================

Concept
--------

Plants distribute about 20% of their photosynthesized carbon into the rhizosphere
through root exudation. Therefore, the catabolic efficiency to utilize the root
exudates is an important determinant on rhizosphere competence (the power of
rhizobacteria to colonize the rhizosphere).

Based on this concept, we developed the rhizoSMASH to detect catabolic gene
clusters that encode enzymes to degrade metabolites that are exudated by plant
roots. Our collection came from:

1. metabolites whose utilization are related to rhizosphere colonization rate
2. gene clusters whose expression are activated when treated with root exudates
3. enriched compounds found in the rhizosphere secreted by plant roots

The presence/absence of these gene cluster in a bacterial genome reflects the
catabolic capacity of the bacteria, hence is predictive to its rhizosphere
competence.


Development & Related Projects
------------------------------

RhizoSMASH is flavour of the secondary metabolite biosynthetic gene cluster
detection tool - __antiSMASH__. We applied the framework of antiSMASH for
pHHM-based gene finding and detection rule-based gene cluster detection.
Following the development of antiSMASH, the development of rhizoSMASH was/is
also under the Bioinformatics group at Wageningen University.

The project was/is under the Sino-ducth Agricultural Green Development programme.
The PhD studenct in the project was/is supported by the China Scholarship Concil
and the graduate school Experimental Plant Sciences.


Installation
------------

### installation on Linux servers (with conda)

First, make a conda environment with name rhizosmash (or other name you want),
and activate it.

```bash
conda create -n rhizosmash python=3.10
conda activate rhizosmash
```

Second, install dependencies with conda.

```bash
conda install hmmer2 hmmer diamond=2.0.15 fasttree prodigal blast muscle glimmerhmm
```

Then, clone the repository and install rhizoSMASH with pip. (__be sure you have
already activated the rhizosmash environment!!__)

```bash
git clone https://git.wur.nl/yuze.li/rhizosmash.git
cd rhizosmash
pip install .
```

Finally, you can download the dependent databases with

```bash
download-rhizosmash-databases
```

### installation hints on Mac OS with apple arm64 chips

This is a short note about installing rhizoSMASH on Mac OS with the new __arm64__
chip. Most dependencies are not avaliable in the osx-arm64 subchannel, but
installing them in the osx-64 subchannel does not cause a problem. Therefore to
work with new osx-arm64, you need switch to the osx-64 subchannel before installing
the dependencies.

(Note: install python3 after the shift, and set the subdir only inside the env)

```bash
conda create -n rhizosmash
conda activate rhizosmash
conda config --env --set subdir osx-64
conda install python=3.10
conda install hmmer2 hmmer diamond=2.0.15 fasttree prodigal blast muscle glimmerhmm
```

The rest steps are the same as in Linux servers.


Running rhizoSMASH
------------------

The rhizosmash repository clone came with a test genome (_Pseudomonas putida_
G1). Try rhizosmash on this genome.

```bash
rhizosmash test.gbk --output-dir test --cb-knownclusters
```

Current Version: 0.3.1
----------------------

### Recent Updates

**17-Nov-2024**

* fix some rules
* add antismash version comment on output genbank for bigscape recognition

**6-Nov-2024**

* rhizoSMASH v0.3, tuning for first round
* Category "Phytohormone" added
* Optimize UI for hybrid regions

**20-Mar-2024**

* rKnownCGC database modified
* Now, only CDS and gene features will be splitted if they are crossing the origin.
* Detection rules re-ordered according to their substrate

**12-Feb-2024**

* CDS name collision "multiple CDS features have the same name" error solved. RhizoSMASH will add ".1", ".2" etc. to gene names of CDS features to avoid name collision.

**6-Feb-2024**

* Known cluster blast is now available (rKnownCGC database v1.0). You can enable it by turning on the `--cb-knownclusters` option.
* New cluster detection rules for biogenic amines and some other secondary metabolites are added.
* Cluster names are updated, making them more informatic.
* Unnecessary modules are removed.

### Notes

* A small mock cluster blast database will also be downloaded by the `download-rhizosmash-databases`. But this dataset is not functional. Pleas not turn on the `--cb-general` and `--cb-subclusters` options.
* Detection rules and pHMMs are not well tuned at this moment. This version is only for testing. Tunned detection rules and pHMMs will be updated on v1.0.

Acknowledgements
----------------

Some icons used courtesy of fontawesome.com
