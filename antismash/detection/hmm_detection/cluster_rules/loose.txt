# Contains rules for poorly defined clusters and/or clusters which are likely to
# match primary metabolism

# Cutoffs and neighbourhoods are given in kilobases

RULE Gamma-glutamyl_transferase
    CATEGORY        Amino-acid
    DESCRIPTION     ggt
    CUTOFF          3
    NEIGHBOURHOOD   5
    CONDITIONS g_glut_trans