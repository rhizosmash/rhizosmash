2A0302	JCVI: basic amino acid/polyamine antiporter	382	TIGR00905.1.HMM
3HCDH	3-hydroxyacyl-CoA dehydrogenase, C-terminal domain	22	PF00725.25.hmm
3HCDH_N	3-hydroxyacyl-CoA dehydrogenase, NAD binding domain	22	PF02737.21.hmm
4OH_2_O_val_ald	JCVI: 4-hydroxy-2-oxovalerate aldolase	147	TIGR03217.1.HMM
AA-adenyl-dom	JCVI: amino acid adenylation domain	254	TIGR01733.1.HMM
AMP-binding	AMP-binding enzyme	29	PF00501.31.hmm
AMP-binding_C	AMP-binding enzyme C-terminal domain	24	PF13193.9.hmm
AMP-binding_C_2	AMP-binding enzyme C-terminal domain	25	PF14535.9.hmm
AP_endonuc_2	Xylose isomerase-like TIM barrel	31	PF01261.27.hmm
Abhydrolase_1	alpha/beta hydrolase fold	25	PF00561.23.hmm
Acyl-CoA_dh_1	Acyl-CoA dehydrogenase, C-terminal domain	22	PF00441.27.hmm
Acyl-CoA_dh_2	Acyl-CoA dehydrogenase, C-terminal domain	22	PF08028.14.hmm
Acyl-CoA_dh_M	Acyl-CoA dehydrogenase, middle domain	33	PF02770.22.hmm
Acyl-CoA_dh_N	Acyl-CoA dehydrogenase, N-terminal domain	22	PF02771.19.hmm
Ald_Xan_dh_C	Aldehyde oxidase and xanthine dehydrogenase, a/b hammerhead domain	31	PF01315.25.hmm
Aldedh	Aldehyde dehydrogenase family	23	PF00171.25.hmm
Aldolase_II	Class II Aldolase and Adducin N-terminal domain	22	PF00596.24.hmm
Alpha-amylase	Alpha amylase, catalytic domain	22	PF00128.27.hmm
Amdase	Arylmalonate decarboxylase	25	PF17645.4.hmm
Amidase	Amidase	27	PF01425.24.hmm
Amidohydro_2	Amidohydrolase	26	PF04909.17.hmm
Amino_oxidase	Flavin containing amine oxidoreductase	19	PF01593.27.hmm
Aminotran_1_2	Aminotransferase class I and II	19	PF00155.24.hmm
Aminotran_3	Aminotransferase class-III	30	PF00202.24.hmm
BenC	NCBIFAM: benzoate 1,2-dioxygenase electron transfer component BenC	430	NF040810.1.HMM
BenD	NCBIFAM: benzoate diol dehydrogenase BenD	380	NF040811.1.HMM
Beta_helix	Right handed beta helix region	25	PF13229.9.hmm
CBM9_1	Carbohydrate family 9 binding domain-like	23	PF06452.14.hmm
CHMI	5-carboxymethyl-2-hydroxymuconate isomerase	26	PF02962.18.hmm
CN_hydrolase	Carbon-nitrogen hydrolase	23	PF00795.25.hmm
CO_deh_flav_C	CO dehydrogenase flavoprotein C-terminal domain	25	PF03450.20.hmm
CoA_transf_3	CoA-transferase family III	23	PF02515.20.hmm
CsiD	CsiD	27	PF08943.13.hmm
Cu_amine_oxid	Copper amine oxidase, enzyme domain	27	PF01179.23.hmm
Cu_amine_oxidN1	Copper amine oxidase N-terminal domain	22	PF07833.14.hmm
Cu_amine_oxidN2	Copper amine oxidase, N2 domain	21	PF02727.19.hmm
Cu_amine_oxidN3	Copper amine oxidase, N3 domain	22	PF02728.19.hmm
Cupin_2	Cupin domain	27	PF07883.14.hmm
Cytochrom_C	Cytochrome c	21	PF00034.24.hmm
Cytochrome_CBB3	Cytochrome C oxidase, cbb3-type, subunit III	27	PF13442.9.hmm
D1pyr5carbox2	JCVI: L-glutamate gamma-semialdehyde dehydrogenase	443	TIGR01237.1.HMM
D1pyr5carbox3	JCVI: L-glutamate gamma-semialdehyde dehydrogenase	374	TIGR01238.1.HMM
Na_Pro_sym	JCVI: sodium/proline symporter PutP	261	TIGR02121.1.HMM
DAO	FAD dependent oxidoreductase	27	PF01266.27.hmm
DHDPS	Dihydrodipicolinate synthetase family	22	PF00701.25.hmm
DHquinase_I	Type I 3-dehydroquinase	22	PF01487.18.hmm
DHquinase_II	Dehydroquinase class II	23	PF01220.22.hmm
DSBA	DSBA-like thioredoxin domain	25	PF01323.23.hmm
DUF6537	Family of unknown function (DUF6537)	27	PF20169.1.hmm
D_pro_red_PrdA	JCVI: D-proline reductase (dithiol) proprotein PrdA	350	TIGR04480.1.HMM
D_pro_red_PrdB	JCVI: D-proline reductase (dithiol) protein PrdB	225	TIGR04483.1.HMM
Dehyd-heme_bind	Quinohemoprotein amine dehydrogenase A, alpha subunit, haem binding	25	PF09098.13.hmm
ECH_1	Enoyl-CoA hydratase/isomerase	27	PF00378.23.hmm
Epimerase	NAD dependent epimerase/dehydratase family	20	PF01370.24.hmm
FAA_hydrolase	Fumarylacetoacetate (FAA) hydrolase family	23	PF01557.21.hmm
FAD_binding_1	FAD binding domain	32	PF00667.23.hmm
FAD_binding_3	FAD binding domain	22	PF01494.22.hmm
FAD_binding_5	FAD binding domain in molybdopterin dehydrogenase	27	PF00941.24.hmm
FAD_binding_6	Oxidoreductase FAD-binding domain	21	PF00970.27.hmm
LysR_substrate	LysR substrate binding domain	28	PF03466.23.hmm
MFS_1	Major Facilitator Superfamily	33	PF07690.19.hmm
Fer2	2Fe-2S iron-sulfur cluster binding domain	21	PF00111.30.hmm
Fer2_2	[2Fe-2S] binding domain	30	PF01799.23.hmm
Fer4	4Fe-4S binding domain	21	PF00037.30.hmm
Flavin_Reduct	Flavin reductase like domain	27	PF01613.21.hmm
Glutaminase	Glutaminase	27	PF04960.18.hmm
Asparaginase	Asparaginase, N-terminal	23	PF00710.23.hmm
HATPase_c	Histidine kinase-, DNA gyrase B-, and HSP90-like ATPase	26	PF02518.29.hmm
Response_reg	Response regulator receiver domain	30	PF00072.27.hmm
Na_Ala_symp	Sodium:alanine symporter family	24	PF01235.20.hmm
GABAtrnsam	JCVI: 4-aminobutyrate--2-oxoglutarate transaminase	317	TIGR00700.1.HMM
GATase_2	Glutamine amidotransferases class-II	19	PF00310.24.hmm
GOGAT_sm_gam	JCVI: glutamate synthase small subunit	370	TIGR01317.1.HMM
GST_N_3	Glutathione S-transferase, N-terminal domain	22	PF13417.9.hmm
GXGXG	GXGXG motif	32	PF01493.22.hmm
Gln-synt_C	Glutamine synthetase, catalytic domain	24	PF00120.27.hmm
Gln-synt_N	Glutamine synthetase, beta-Grasp domain	33	PF03951.22.hmm
Gln_ase	JCVI: glutaminase A	246	TIGR03814.1.HMM
Glu_syn_central	Glutamate synthase central domain	22	PF04898.17.hmm
Glu_synthase	Conserved region in glutamate synthase	20	PF01645.20.hmm
Glu_dh	PANTHER: Glutamate dehydrogenase	138	PTHR11606.hmm
ELFV_dehydrog	Glutamate/Leucine/Phenylalanine/Valine dehydrogenase	23	PF00208.24.hmm
ELFV_dehydrog_N	Glu/Leu/Phe/Val dehydrogenase, dimerisation domain	25	PF02812.21.hmm
Glyco_hydro_3	Glycosyl hydrolase family 3 N terminal domain	27	PF00933.24.hmm
Glyco_hydro_3_C	Glycosyl hydrolase family 3 C-terminal domain	31	PF01915.25.hmm
Glyco_hydro_10	Glycosyl hydrolase family 10	22	PF00331.23.hmm
Glyco_hydro_11	Glycosyl hydrolases family 11	27	PF00457.20.hmm
Glyco_hydro_30	Glycosyl hydrolase family 30 TIM-barrel domain	23	PF02055.19.hmm
Glyco_hydro_30C	Glycosyl hydrolase family 30 beta sandwich domain	22	PF17189.7.hmm
Glyco_hydro_32C	Glycosyl hydrolases family 32 C terminal	21	PF08244.15.hmm
Glyco_hydro_32N	Glycosyl hydrolases family 32 N-terminal domain	22	PF00251.23.hmm
Glyco_hydro_39	Glycosyl hydrolases family 39	26	PF01229.20.hmm
Glyco_hydro_43	Glycosyl hydrolases family 43	32	PF04616.17.hmm
Alpha-L-AF_C	Alpha-L-arabinofuranosidase C-terminal domain (GH51)	27	PF06964.15.hmm
GH43_C2	Beta xylosidase C-terminal Concanavalin A-like domain	22	PF17851.4.hmm
Glyco_hydro_65C	Glycosyl hydrolase family 65, C-terminal domain	22	PF03633.18.hmm
Glyco_hydro_65N	Glycosyl hydrolase family 65, N-terminal domain	26	PF03636.18.hmm
Glyco_hydro_65m	Glycosyl hydrolase family 65 central catalytic domain	27	PF03632.18.hmm
Glyco_hydro_67N	Glycosyl hydrolase family 67 N-terminus	24	PF03648.17.hmm
Glyco_hydro_67M	Glycosyl hydrolase family 67 middle domain	23	PF07488.15.hmm
Glyco_hydro_67C	Glycosyl hydrolase family 67 C-terminus	27	PF07477.15.hmm
Glyco_hydro_68	Levansucrase/Invertase	27	PF02435.19.hmm
Glyco_hydro_115	Glycosyl hydrolase family 115	27	PF15979.8.hmm
GH115_C	Gylcosyl hydrolase family 115 C-terminal domain	24	PF17829.4.hmm
CBM_6	Carbohydrate binding module (family 6)	28	PF03422.18.hmm
UxaC	Glucuronate isomerase	25	PF02614.17.hmm
AXE1	Acetyl xylan esterase (AXE1)	21	PF05448.15.hmm
CE2_N	Carbohydrate esterase 2 N-terminal	26	PF17996.4.hmm
Esterase_PHB	Esterase PHB depolymerase	23	PF10503.12.hmm
BD-FAE	BD-FAE	26	PF20434.1.hmm
Glyoxalase	Glyoxalase/Bleomycin resistance protein/Dioxygenase superfamily	20	PF00903.28.hmm
HpaA	JCVI: 4-hydroxyphenylacetate catabolism regulatory protein HpaA	197	TIGR02297.1.HMM
HpaB	4-hydroxyphenylacetate 3-hydroxylase C terminal	25	PF03241.16.hmm
HpaB-1	JCVI: 4-hydroxyphenylacetate 3-monooxygenase, oxygenase component	332	TIGR02309.1.HMM
HpaB-2	JCVI: 4-hydroxyphenylacetate 3-monooxygenase, oxygenase component	741	TIGR02310.1.HMM
HpaB_N	4-hydroxyphenylacetate 3-hydroxylase N terminal	35	PF11794.11.hmm
HpaC	JCVI: 4-hydroxyphenylacetate 3-monooxygenase, reductase component	178	TIGR02296.1.HMM
HpaD	JCVI: 3,4-dihydroxyphenylacetate 2,3-dioxygenase	264	TIGR02295.1.HMM
HpaD_Fe	JCVI: 3,4-dihydroxyphenylacetate 2,3-dioxygenase	127	TIGR02298.1.HMM
HpaE	JCVI: 5-carboxymethyl-2-hydroxymuconate semialdehyde dehydrogenase	593	TIGR02299.1.HMM
HpaG-C-term	JCVI: 4-hydroxyphenylacetate degradation bifunctional isomerase/decarboxylase, C-terminal subunit	242	TIGR02303.1.HMM
HpaG-N-term	JCVI: 4-hydroxyphenylacetate degradation bifunctional isomerase/decarboxylase, N-terminal subunit	203	TIGR02305.1.HMM
HpaH	JCVI: 2-oxo-hepta-3-ene-1,7-dioic acid hydratase	311	TIGR02312.1.HMM
HpaI	JCVI: 4-hydroxy-2-oxoheptanedioate aldolase	360	TIGR02311.1.HMM
HpaX	JCVI: 4-hydroxyphenylacetate permease	527	TIGR02332.1.HMM
HpcH_HpaI	HpcH/HpaI aldolase/citrate lyase family	25	PF03328.17.hmm
Hydant_A_C	Hydantoinase/oxoprolinase C-terminal domain	21	PF19278.2.hmm
Hydant_A_N	Hydantoinase/oxoprolinase N-terminal region	25	PF05378.16.hmm
Hydantoinase_A	Hydantoinase/oxoprolinase	22	PF01968.21.hmm
Hydantoinase_B	Hydantoinase B/oxoprolinase	26	PF02538.17.hmm
Isochorismatase	Isochorismatase family	27	PF00857.23.hmm
LAT_fam	JCVI: L-lysine 6-transaminase	271	TIGR03251.1.HMM
Ldh_2	Malate/L-lactate dehydrogenase	38	PF02615.17.hmm
LigA	Aromatic-ring-opening dioxygenase LigAB, LigA subunit	25	PF07746.14.hmm
LigB	Catalytic LigB subunit of aromatic ring-opening dioxygenase	26	PF02900.21.hmm
GFO_IDH_MocA	Oxidoreductase family, NAD-binding Rossmann fold	24	PF01408.25.hmm
OxRdtase_C	Bacterial oxidoreductases, C-terminal	26	PF19858.2.hmm
MMSDH	JCVI: CoA-acylating methylmalonate-semialdehyde dehydrogenase	392	TIGR01722.1.HMM
Mannitol_dh	Mannitol dehydrogenase Rossmann domain	27	PF01232.26.hmm
Mannitol_dh_C	Mannitol dehydrogenase C-terminal domain	30	PF08125.16.hmm
MmoB_DmpM	MmoB/DmpM family	25	PF02406.20.hmm
MoCoBD_1	Molybdopterin cofactor-binding domain	27	PF02738.21.hmm
MoCoBD_2	Molybdopterin cofactor-binding domain	27	PF20256.1.hmm
NAD_binding_1	Oxidoreductase NAD-binding domain	22	PF00175.24.hmm
NAD_binding_2	NAD binding domain of 6-phosphogluconate dehydrogenase	21	PF03446.18.hmm
NAD_binding_11	NAD-binding of NADP-dependent 3-hydroxyisobutyrate dehydrogenase	29	PF14833.9.hmm
NBD_C	Nucleotide-binding C-terminal domain	28	PF17042.8.hmm
OCD_Mu_crystall	Ornithine cyclodeaminase/mu-crystallin family	24	PF02423.18.hmm
OH_muco_semi_DH	JCVI: 2-hydroxymuconic semialdehyde dehydrogenase	576	TIGR03216.1.HMM
Oxidored_FMN	NADH:flavin oxidoreductase / NADH oxidase family	27	PF00724.23.hmm
PA_CoA_Oxy1	JCVI: 1,2-phenylacetyl-CoA epoxidase subunit PaaA	219	TIGR02156.2.HMM
PA_CoA_Oxy2	JCVI: 1,2-phenylacetyl-CoA epoxidase subunit PaaB	42	TIGR02157.1.HMM
PA_CoA_Oxy3	JCVI: 1,2-phenylacetyl-CoA epoxidase subunit PaaC	79	TIGR02158.1.HMM
PA_CoA_Oxy4	JCVI: 1,2-phenylacetyl-CoA epoxidase subunit PaaD	74	TIGR02159.1.HMM
PA_CoA_Oxy5	JCVI: 1,2-phenylacetyl-CoA epoxidase subunit PaaE	409	TIGR02160.1.HMM
PaaX	PaaX-like protein	50	PF07848.15.hmm
PaaX_C	PaaX-like protein C-terminal domain	60	PF08223.14.hmm
PaaY	phenylacetic acid degradation protein PaaY	304	TIGR02287.1.HMM
Hexapep	Bacterial transferase hexapeptide (six repeats)	27	PF00132.27.hmm
PaaC-3OHAcCoADH	3-hydroxyacyl-CoA dehydrogenase PaaH	593	TIGR02279.1.HMM
PaaA_PaaC	Phenylacetic acid catabolic protein	200	PF05138.15.hmm
PaaB	Phenylacetic acid degradation B	27	PF06243.14.hmm
FeS_assembly_P	Iron-sulfur cluster assembly protein	23	PF01883.22.hmm
PA_CoA_ligase	JCVI: phenylacetate--CoA ligase PaaK	597	TIGR02155.1.HMM
POR	Pyruvate ferredoxin/flavodoxin oxidoreductase	27	PF01558.21.hmm
PQQ_membr_DH	JCVI: membrane-bound PQQ-dependent dehydrogenase, glucose/quinate/shikimate family	552	TIGR03074.1.HMM
PRODH	Proline utilization A proline dehydrogenase N-terminal domain	26	PF18327.4.hmm
PR_assoc_PrdC	JCVI: proline reductase-associated electron transfer protein PrdC	300	TIGR04481.1.HMM
PTS_EIIB	phosphotransferase system, EIIB	27	PF00367.23.hmm
PTS_EIIC	Phosphotransferase system, EIIC	30	PF02378.21.hmm
PaaB1	JCVI: 2-(1,2-epoxy-1,2-dihydrophenyl)acetyl-CoA isomerase PaaG	303	TIGR02280.1.HMM
PaaD	JCVI: hydroxyphenylacetyl-CoA thioesterase PaaI	77	TIGR02286.1.HMM
PaaN-DH	JCVI: phenylacetic acid degradation bifunctional protein PaaZ	232	TIGR02278.1.HMM
PdxA	Pyridoxal phosphate biosynthetic protein PdxA	27	PF04166.15.hmm
Peptidase_C26	Peptidase C26	20	PF07722.16.hmm
PfkB	pfkB family carbohydrate kinase	25	PF00294.27.hmm
Phenol_Hydrox	Methane/Phenol/Toluene Hydroxylase	24	PF02332.21.hmm
Phenol_hyd_sub	Phenol hydroxylase subunit	25	PF06099.14.hmm
Phenol_monoox	Phenol hydroxylase conserved region	27	PF04663.15.hmm
Pro_dh	Proline dehydrogenase	27	PF01619.21.hmm
Pro_dh-DNA_bdg	DNA-binding domain of Proline dehydrogenase	27	PF14850.9.hmm
Pro_racemase	Proline racemase	27	PF05544.14.hmm
Pyr_redox_2	Pyridine nucleotide-disulphide oxidoreductase	26	PF07992.17.hmm
QH-AmDH_gamma	Quinohemoprotein amine dehydrogenase, gamma subunit	25	PF08992.14.hmm
QH_alpha	JCVI: quinohemoprotein amine dehydrogenase subunit alpha	273	TIGR03908.1.HMM
QH_beta	JCVI: quinohemoprotein amine dehydrogenase subunit beta	222	TIGR03907.1.HMM
Qn_am_d_aII	Quinohemoprotein amine dehydrogenase, alpha subunit domain II	27	PF14930.9.hmm
Qn_am_d_aIII	Quinohemoprotein amine dehydrogenase, alpha subunit domain III	22	PF09099.13.hmm
Qn_am_d_aIV	Quinohemoprotein amine dehydrogenase, alpha subunit domain IV	25	PF09100.13.hmm
Radical_SAM	Radical SAM superfamily	29	PF04055.24.hmm
RbsD_FucU	RbsD / FucU transport protein family	25	PF05025.16.hmm
Reductase_C	Reductase C-terminal	25	PF14759.9.hmm
MarR	MarR family	24	PF01047.25.hmm
Rieske	Rieske [2Fe-2S] domain	20	PF00355.29.hmm
Ring_hydroxyl_A	Ring hydroxylating alpha subunit (catalytic domain)	24	PF00848.22.hmm
Ring_hydroxyl_B	Ring hydroxylating beta subunit	27	PF00866.21.hmm
SBD_N	Sugar-binding N-terminal domain	25	PF07005.14.hmm
SBP_bac_3	Bacterial extracellular solute-binding proteins, family 3	25	PF00497.23.hmm
SDH_C	Shikimate 5'-dehydrogenase C-terminal domain	25	PF18317.4.hmm
SSADH	JCVI: succinate-semialdehyde dehydrogenase	499	TIGR01780.1.HMM
Shikimate_dh_N	Shikimate dehydrogenase substrate binding domain	27	PF08501.14.hmm
SnoaL_4	SnoaL-like domain	24	PF13577.9.hmm
TPP_enzyme_C	Thiamine pyrophosphate enzyme, C-terminal TPP binding domain	21	PF02775.24.hmm
Thiolase_C	Thiolase, C-terminal domain	27	PF02803.21.hmm
Thiolase_N	Thiolase, N-terminal domain	26	PF00108.26.hmm
TmoB	Toluene-4-monooxygenase system protein B (TmoB)	25	PF06234.15.hmm
Trehalase	Trehalase	21	PF01204.23.hmm
Glyco_hydro_15	Glycosyl hydrolases family 15	22	PF00723.24.hmm
TREH_N	Trehalase-like, N-terminal	23	PF19291.2.hmm
Trehalose_PPase	Trehalose-phosphatase	27	PF02358.19.hmm
bPGM	JCVI: beta-phosphoglucomutase	146	TIGR01990.1.HMM
YgjK_N	Glucosidase YgjK, N-terminal	27	PF21152.2.hmm
VanA_C	Vanillate O-demethylase oxygenase C-terminal domain	23	PF19112.3.hmm
XylB	JCVI: xylulokinase	368	TIGR01312.1.HMM
YHS	YHS domain	30	PF04945.16.hmm
B12-binding	B12 binding domain	32	PF02310.22.hmm
MM_CoA_mutase	Methylmalonyl-CoA mutase	22	PF01642.25.hmm
ac_ald_DH_ac	JCVI: acetaldehyde dehydrogenase (acetylating)	98	TIGR03215.1.HMM
acid_CoA_mut_C	JCVI: methylmalonyl-CoA mutase C-terminal domain	27	TIGR00640.1.HMM
acid_CoA_mut_N	JCVI: methylmalonyl-CoA mutase N-terminal domain	136	TIGR00641.1.HMM
adh_short	short chain dehydrogenase	26	PF00106.28.hmm
adh_short_C2	Enoyl-(Acyl carrier protein) reductase	27	PF13561.9.hmm
agcS	JCVI: amino acid carrier protein	259	TIGR00835.1.HMM
agmatine_aguA	JCVI: agmatine deiminase	309	TIGR03380.1.HMM
agmatine_aguB	JCVI: N-carbamoylputrescine amidase	304	TIGR03381.1.HMM
asnASE_II	JCVI: type II asparaginase	211	TIGR00520.1.HMM
benzo_1_2_benA	JCVI: benzoate 1,2-dioxygenase large subunit	566	TIGR03229.1.HMM
benzo_1_2_benB	JCVI: benzoate 1,2-dioxygenase small subunit	199	TIGR03232.1.HMM
catechol_2_3	JCVI: catechol 2,3-dioxygenase	237	TIGR03211.1.HMM
catechol_dmpE	JCVI: 2-oxopent-4-enoate hydratase	247	TIGR03220.1.HMM
catechol_dmpH	JCVI: 2-oxo-3-hexenedioate decarboxylase	294	TIGR03218.1.HMM
catechol_proteo	JCVI: catechol 1,2-dioxygenase	192	TIGR02439.1.HMM
Dioxygenase_N	Catechol dioxygenase N terminus	27	PF04444.17.hmm
MR_MLE_N	Mandelate racemase / muconate lactonizing enzyme, N-terminal domain	22	PF02746.19.hmm
MR_MLE_C	Enolase C-terminal domain-like	29	PF13378.9.hmm
MIase	Muconolactone delta-isomerase	26	PF02426.19.hmm
custom_aminoadipate_dh	iacE	450	pthr_aminoadipate_dh.hmm
custom_cadA	L-lys decarboxylase	400	cadA_init.hmm
glutaryl_succinyl_coat	Succinyl-CoA--glutaryl CoA transferase	335	PTHR48207.hmm
glutarylcoa_dh	Glutaryl-CoA dehydrogenase	450	PTHR42807.hmm
custom_iacA	iacA	200	IacA_init.hmm
custom_iacB	iacB	130	IacB_init.hmm
custom_iacC	iacC	500	IacC_init.hmm
custom_iacD	iacD	150	IacD_init.hmm
custom_iacE	iacE	270	IacE_init.hmm
custom_iacF	iacF	350	IacF_init.hmm
custom_nagG	Salicylate 5-hydrolase	200	nagG_init.hmm
custom_nagI	Gentistate 1,2-dioxygenase	300	nagI_init.hmm
custom_nicA	Nicotinate 6-hydroxylase Fe-S small subunit	180	nicA_init.hmm
custom_nicX	Salicylyl-CoA Transferase A	190	nicX_init.hmm
custom_sdgA	Salicylyl-CoA Transferase A	100	sdgA_init.hmm
ATP-grasp_5	ATP-grasp domain	27	PF13549.9.hmm
Succ_CoA_lig	Succinyl-CoA ligase like flavodoxin domain	26	PF13607.9.hmm
CoA_binding_2	CoA binding domain	22	PF13380.9.hmm
custom_sdgB	Salicylyl-CoA Transferase B	20	sdgB_init.hmm
custom_sdgC	Salicylyl-CoA Dehydrogenase	300	sdgC_init.hmm
MDMPI_N	Mycothiol maleylpyruvate isomerase N-terminal domain	24	PF11716.11.hmm
g_glut_trans	JCVI: gamma-glutamyltransferase	182	TIGR00066.1.HMM
gltA	JCVI: NADPH-dependent glutamate synthase	394	TIGR01316.1.HMM
gltD_gamma_fam	JCVI: glutamate synthase, small subunit	399	TIGR01318.1.HMM
gph	JCVI: glycoside-pentoside-hexuronide (GPH):cation symporter	219	TIGR00792.1.HMM
indol_phenyl_DC	JCVI: indolepyruvate/phenylpyruvate decarboxylase	464	TIGR03394.1.HMM
indolpyr_decarb	JCVI: indolepyruvate decarboxylase	696	TIGR03393.1.HMM
maiA	JCVI: maleylacetoacetate isomerase	104	TIGR01262.1.HMM
mtlA	JCVI: mannitol-specific PTS transporter subunit IIC	398	TIGR00851.1.HMM
PTS_IIB	PTS system, Lactose/Cellobiose specific IIB subunit	23	PF02302.20.hmm
PTS_EIIA_2	Phosphoenolpyruvate-dependent sugar phosphotransferase system, EIIA 2	27	PF00359.25.hmm
ABC_tran	ABC transporter	25	PF00005.30.hmm
muco_delta	JCVI: muconolactone Delta-isomerase	33	TIGR03221.1.HMM
mucon_cyclo	JCVI: muconate/chloromuconate family cycloisomerase	276	TIGR02534.1.HMM
myo_inos_iolB	JCVI: 5-deoxy-glucuronate isomerase	180	TIGR04378.1.HMM
myo_inos_iolC_N	JCVI: 5-dehydro-2-deoxygluconokinase	270	TIGR04382.1.HMM
myo_inos_iolD	JCVI: 3D-(3,5/4)-trihydroxycyclohexane-1,2-dione acylhydrolase (decyclizing)	695	TIGR04377.1.HMM
myo_inos_iolE	JCVI: myo-inosose-2 dehydratase	250	TIGR04379.1.HMM
myo_inos_iolG	JCVI: inositol 2-dehydrogenase	330	TIGR04380.1.HMM
F_bP_aldolase	Fructose-bisphosphate aldolase class-II	25	PF01116.23.hmm
pbenz_hydroxyl	JCVI: 4-hydroxybenzoate 3-monooxygenase	219	TIGR02360.1.HMM
pcaF	JCVI: 3-oxoadipyl-CoA thiolase	501	TIGR02430.1.HMM
MaoC_dehydratas	MaoC like domain	27	PF01575.22.hmm
pcaI_scoA_fam	JCVI: 3-oxoacid CoA-transferase subunit A	103	TIGR02429.1.HMM
pcaJ_scoB_fam	JCVI: 3-oxoacid CoA-transferase subunit B	77	TIGR02428.1.HMM
protocat_alph	JCVI: protocatechuate 3,4-dioxygenase subunit alpha	123	TIGR02423.1.HMM
protocat_beta	JCVI: protocatechuate 3,4-dioxygenase subunit beta	144	TIGR02422.1.HMM
Dioxygenase_C	Dioxygenase	27	PF00775.24.hmm
protocat_pcaB	JCVI: 3-carboxy-cis,cis-muconate cycloisomerase	298	TIGR02426.1.HMM
Lyase_1	Lyase	27	PF00206.23.hmm
decarb_PcaC	JCVI: 4-carboxymuconolactone decarboxylase	138	TIGR02425.1.HMM
CMD	Carboxymuconolactone decarboxylase family	23	PF02627.23.hmm
protocat_pcaD	JCVI: 3-oxoadipate enol-lactonase	179	TIGR02427.1.HMM
putres_am_tran	JCVI: putrescine aminotransferase	467	TIGR03372.1.HMM
quino_hemo_SAM	JCVI: quinohemoprotein amine dehydrogenase maturation protein	451	TIGR03906.1.HMM
rSAM_more_4Fe4S	JCVI: SPASM domain	16	TIGR04085.1.HMM
salicylate_mono	JCVI: salicylate 1-monooxygenase	353	TIGR03219.1.HMM
scrB_fam	JCVI: sucrose-6-phosphate hydrolase	342	TIGR01322.1.HMM
sss	JCVI: sodium/solute symporter	149	TIGR00813.1.HMM
sucrose_gtfA	JCVI: sucrose phosphorylase	240	TIGR03852.1.HMM
taut	JCVI: 2-hydroxymuconate tautomerase family protein	31	TIGR00013.1.HMM
trehalose_treC	JCVI: alpha,alpha-phosphotrehalase	751	TIGR02403.1.HMM
xylose_isom_A	JCVI: xylose isomerase	164	TIGR02630.1.HMM
Cyclase	Putative cyclase	23	PF04199.16.hmm
Malt_amylase_C	Malt_amylase_C	27	PF16657.8.hmm
EII-Sor	PTS system sorbose-specific iic component	26	PF03609.17.hmm
EIIA-man	PTS system fructose IIA component	21	PF03610.19.hmm
PTSIIB_sorb	PTS system sorbose subfamily IIB component	26	PF03830.18.hmm
EIID-AGA	PTS system mannose/fructose/sorbose family IID component	25	PF03613.17.hmm
OKR_DC_1	Orn/Lys/Arg decarboxylase, major domain	27	PF01276.23.hmm
OKR_DC_1_C	Orn/Lys/Arg decarboxylase, C-terminal domain	25	PF03711.18.hmm
OKR_DC_1_N	Orn/Lys/Arg decarboxylase, N-terminal domain	30	PF03709.18.hmm
AA_permease_2	Amino acid permease	27	PF13520.9.hmm
HGLS	2-oxoadipate dioxygenase/decarboxylase	25	PF07063.18.hmm
2OG-FeII_Oxy	2OG-Fe(II) oxygenase superfamily	23	PF03171.23.hmm
GRDB	Glycine/sarcosine/betaine reductase selenoprotein B (GRDB)	22	PF07355.15.hmm
Dehydratase_hem	Haem-containing dehydratase	25	PF13816.9.hmm
NHase_alpha	Nitrile hydratase, alpha chain	27	PF02979.19.hmm
NHase_beta_N	Nitrile hydratase beta subunit, N-terminal	21	PF21006.2.hmm
NHase_beta_C	Nitrile hydratase beta subunit, C-terminal	27	PF02211.20.hmm
DUF4168	Domain of unknown function (DUF4168)	25	PF13767.11.hmm
Bac_luciferase	Luciferase-like monooxygenase	27	PF00296.25.hmm
g1_2_dioxygenase	1,2-dioxygenase	250	PTHR41517.hmm
gentisate_1_2	JCVI: gentisate 1,2-dioxygenase	226	TIGR02272.1.HMM
Rng_hyd_C	Aromatic-ring hydroxylase, C-terminal	27	PF21274.2.hmm
DinB_2	DinB superfamily	24	PF12867.12.hmm
NosD	Periplasmic copper-binding protein (NosD)	23	PF05048.18.hmm
OmpW	OmpW family	24	PF03922.19.hmm
PQQ_2	PQQ-like domain	27	PF13360.11.hmm
PQQ	PQQ enzyme repeat	20	PF01011.26.hmm
Transthyretin	HIUase/Transthyretin family	25	PF00576.26.hmm
Uricase	Uricase	24	PF01014.23.hmm
XdhC_C	XdhC Rossmann domain	29	PF13478.11.hmm
ant_diox_AndAc	NCBIFAM: anthranilate 1,2-dioxygenase large subunit AndAc	750	NF041684.1.HMM
ant_diox_AndAd	NCBIFAM: anthranilate 1,2-dioxygenase small subunit AndAd	230	NF041685.1.HMM
anthran_1_2_B	JCVI: anthranilate 1,2-dioxygenase small subunit	176	TIGR03231.1.HMM
anthran_1_2_A	JCVI: anthranilate 1,2-dioxygenase large subunit	671	TIGR03228.1.HMM
Aromatic_hydrox	Homotrimeric ring hydroxylase	24	PF11723.13.hmm
LigXa_C	LigXa C-terminal domain like	26	PF19301.4.hmm
AcetDehyd-dimer	Prokaryotic acetaldehyde dehydrogenase, dimerisation	26	PF09290.16.hmm
DmpG_comm	DmpG-like communication domain	27	PF07836.16.hmm
HMGL-like	HMGL-like	29	PF00682.24.hmm
PCDO_beta_N	Protocatechuate 3,4-dioxygenase beta subunit N terminal	24	PF12391.13.hmm
Semialdhyde_dh	Semialdehyde dehydrogenase, NAD binding domain	26	PF01118.29.hmm
Tautomerase	Tautomerase enzyme	22	PF01361.26.hmm
DmmA-like_N	Dimethylamine monooxygenase subunit DmmA-like, N-terminal domain	33	PF22290.1.hmm
